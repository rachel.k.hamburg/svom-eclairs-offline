# Source the correct virtual environment
# ~/venv/eclairs-subthreshold/bin/activate

import argparse
from astropy.coordinates import SkyCoord
from astropy.io import fits
import astropy.units as u
from configparser import ConfigParser
from datetime import datetime
import glob
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import linregress
import sys
#import tables as tb

from background import *
from models import ModelCounts
from svom_plot import detector_plane_image

def print_header(hdr):
    print ('\n', repr(hdr))
    return

print ("\nSVOM ECLAIRs Subthreshold Search\n")

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--config', default='config.ini', help="path to config.ini file")
parser.add_argument('-t', '--time', default=None, help="input search time")
args = parser.parse_args()

# Get search input time
if args.time is not None:
    input_time = args.time
else:
    input_time = '2020-01-07T21:24:47.000' # grb = T0+1487.5 s
    # start of other sim file = 2025-01-07T04:15:00.000
   
# Get search configuration
config = ConfigParser()
config.read(args.config)

paths = config['paths']
base = paths.get('base')
data_dir = '{}/{}'.format(base, paths.get('data_dir'))
inst_dir = '{}/{}'.format(base, paths.get('inst_dir'))
cat_dir  = '{}/{}'.format(base, paths.get('cat_dir'))

inputs = config['inputs']
search_start = inputs.getfloat('search_start')
search_stop  = inputs.getfloat('search_stop')
bkgd_start   = inputs.getfloat('bkgd_start')
bkgd_stop    = inputs.getfloat('bkgd_stop')
timescales   = inputs.get('timescales')
timescales   = [float(t) for t in timescales.split(',')]
phase        = inputs.getfloat('phase')
bkgd_res     = inputs.getfloat('bkgd_res')
energy_bins  = inputs.get('energy_bins').split("\n")
energy_bins  = [eb.split(',') for eb in energy_bins]
energy_bins  = [[float(eb[0]), float(eb[1])] for eb in energy_bins]

parameters = {}
for p in config.options('initial parameters'):
    parameters[p] = config.get('initial parameters', p)

# Get data file/s

# Read photon event data file
events_file = data_dir+'/events_2020-01-07T21_00_00.000_6000.0_w_GRB_T1500.fits'
events_file = fits.open(events_file)[1]
ph_events   = events_file.data

# Plot lightcurve
bin_edges = np.arange(0, 6000, step=1.024)
counts = np.histogram(ph_events["eventID"], bin_edges)[0]
plt.stairs(counts, edges=bin_edges)
plt.show()
plt.close()

"""
# Read instrument response files
# ARF
# matrix: [low E, high E, A_eff]
arf = inst_dir+'/caldb/arf_effarea_20220515T104100.fits'
arf = fits.open(arf)[1].data
arf = [row[2] for row in arf]
# IRF
# energy bounds are 20 keV and 100 keV
# matrix: [j,i] [199 x 199] (where 100, 100 is on-axis)
# irf[0] - 20 keV, irf[1] - 100 keV
irf = inst_dir+'/caldb/irf_matrix_20211011T090600.fits'
irf = fits.open(irf)[1].data
# RMF
rmf_file = inst_dir+'/caldb/rmf_matrix_20220515T103600.fits'
rmf_file = fits.open(rmf_file)
rmf = rmf_file[1].data
rmf = np.array([row[5] for row in rmf])
rmf_bnds = rmf_file[2].data

# Get photon event file start and stop times
# and convert to datetime objects
file_start_utc = events_file.header["HIERARCH TSTART_ISOT"]
file_start_utc = datetime.strptime(file_start_utc, '%Y-%m-%dT%H:%M:%S.%f')
file_stop_utc  = events_file.header["HIERARCH TSTOP_ISOT"]
file_stop_utc  = datetime.strptime(file_stop_utc, '%Y-%m-%dT%H:%M:%S.%f')

# Input time relative to photon events file time range
T0_utc = datetime.strptime(input_time, '%Y-%m-%dT%H:%M:%S.%f')
T0 = (T0_utc - file_start_utc).total_seconds()

print ("Input time [UTC]:", T0_utc)
print ("File start [UTC]:", file_start_utc)
print ("File stop [UTC]:", file_stop_utc)

# Set up search time range
search_start = T0 - search_start
search_stop = T0 + search_stop

# Set up background time range
pre_bkgd  = [search_start-bkgd_start, search_start]
post_bkgd = [search_stop, search_stop+bkgd_stop]

print ("\nPre-background: ", pre_bkgd)
print ("Search start time:", search_start)
print ("Search input file time", T0)
print ("Search stop time", search_stop) 
print ("Post-background range:", post_bkgd)

# Get detector solid angles
det_solid_angle = '{}/pixels_solid_angle.fits'.format(inst_dir)
det_solid_angle = fits.open(det_solid_angle)
det_solid_angle = det_solid_angle[0].data

# Get bright catalog sources
bright_sources = '{}/bright_sources.txt'.format(cat_dir)
bright_sources = open(bright_sources, 'r')
bright_sources = [s.strip('\n') for s in bright_sources.readlines()]

# Get sources from ECLAIRs catalog if in the bright catalog file
catalog_file = '{}/J_A+A_645_A18_table.dat.fits'.format(cat_dir)
catalog_file = fits.open(catalog_file)
catalog = catalog_file[1].data
catalog = np.array([s for s in catalog if s["Name"] in bright_sources])

# Swift NITRATES catalog sources
#swift_catalog = fits.open('data/catalog/swift_bright_src_cat.fits')
#swift_pnt_srcs = swift_catalog[1].data

# Plot point sources
eq = SkyCoord(catalog[:,3], catalog[:,4], unit=u.deg)
gal = eq.galactic
plt.subplot(projection='mollweide')
plt.scatter(gal.l.wrap_at('180d').radian, gal.b.radian, s=30)
plt.title('ECLAIRs Onboard Point Source Catalog')
plt.grid()
#plt.show()
plt.close()

# Get attitude information over search duration
attitude_file = '{}/attitude_2020-01-07T21_00_00.000_6000.0.fits'.format(data_dir)
attitude_file = fits.open(attitude_file)
attitude = attitude_file[1].data
attitude = attitude[np.logical_and(
                        attitude["Time"] > pre_bkgd[0],
                        attitude["Time"] < post_bkgd[-1])]

# Get average ECLAIRs pointing
avg_ra  = np.median(attitude["RA(X)"])
avg_dec = np.median(attitude["DEC(X)"])
avg_ecl_pointing = SkyCoord(avg_ra, avg_dec, unit=u.deg)
print ('\nAverage ECLAIRs pointing (RA, Dec):', avg_ra, avg_dec)

# Read sky points file
sky_points = open('data/detector_fractions/sky_points.txt', 'r')
sky_points = [sp.strip('\n') for sp in sky_points.readlines()[1:]]
sky_points = [sp.split() for sp in sky_points]
sky_points = np.array([[float(sp[0]), float(sp[1])] for sp in sky_points])

# Gather detector fraction files
## pretty hacky. should improve
det_frac_path = base+'/data/detector_fractions/'
det_frac_files = glob.glob(det_frac_path+'/*.h5')
s = len(det_frac_path)
det_frac_indices = [int(d[s:].strip('detector_fractions .h5')) for d in det_frac_files]
det_frac_indices = np.sort(det_frac_indices)

# Get point sources in FOV
sources_in_fov = []
"""
"""
for k in range(len(catalog)):
    src_position = SkyCoord(catalog[k][3], catalog[k][4], unit=u.deg)

    # check if source is approx within ECLAIRs FOV
    theta = avg_ecl_pointing.separation(src_position).deg 
    if theta < 50.:

        # Get ECLAIRs full position
        avg_ra_y  = np.median(attitude["RA(Y)"])
        avg_dec_y = np.median(attitude["DEC(Y)"])
        avg_ecl_ypointing = SkyCoord(avg_ra_y, avg_dec_y, unit=u.deg)
        ### The following lines of code came from the SVOM code. 
        ### find and cite, or import and use.
        avg_ecl_zpointing = SkyCoord(
                                avg_ecl_pointing.cartesian.cross(
                                        avg_ecl_ypointing.cartesian))

        # Calculate phi spacecraft coord
        ### The following lines of code came from the SVOM code. 
        ### find and cite, or import and use.
        ysep = avg_ecl_ypointing.separation(src_position).deg
        zsep = avg_ecl_zpointing.separation(src_position).deg
        phi = np.arctan2(np.cos(zsep), np.cos(ysep))
        phi_deg = np.rad2deg(phi)
        print ('Source {} in FOV at instrument theta, phi: {}, {}'.format(
                    catalog[k][0], np.round(theta,3), np.round(phi_deg, 3)
        ))

        # Find closest simulated detector fraction point
        abs_diff = np.abs(np.deg2rad(5.) - sky_points[:,0])
        sky_point_theta = sky_points[np.argmin(abs_diff)][0]
        candidate_points = sky_points[sky_points[:,0] == sky_point_theta]
        if phi < 0:
            phi += 2 * np.pi
        abs_diff = np.abs(phi - candidate_points[:,1])
        sky_point_phi = candidate_points[np.argmin(abs_diff)][1]
        #print (sky_point_theta, sky_point_phi)

        # Find detector fraction file index
        mask = np.logical_and(
                            sky_points[:,0] == sky_point_theta,
                            sky_points[:,1] == sky_point_phi)
        file_idx = [i for i,x in enumerate(mask) if x]     

        # Find detector fraction file
        for d in range(len(det_frac_indices)):
            if det_frac_indices[d] <= file_idx and \
                    det_frac_indices[d+1] > file_idx:
                didx = str(det_frac_indices[d])

        det_frac_file = [f for f in det_frac_files if '_'+didx+'.h5' in f]
        if len(det_frac_file) > 1:
            raise ValueError("There should only be 1 detector file for a source.")

        # Read detector fraction files
        # Format: theta, phi, fractions (80 x 80)
        h5file = tb.open_file(det_frac_file[0], mode='r')
        table = h5file.root.v00.det_frac_table
        point = [x["theta"] for x in table.iterrows()]# if x["theta"][:6] == sky_point_theta[:6]]
        
        print ("Problem with file indices")
        h5file.close()
        # Get fraction of unshadowed detector
        # Append to list
        #sources_in_fov.append(catalog[k])
        sys.exit()
"""
"""

det_frac = None

print ('\nPerforming background fit in off-time intervals...')

# Get off-time bins for background fitting
time_bins = set_bkgd_bins(pre_bkgd, post_bkgd, bkgd_res, off_time=True)

# Map likelihood surface
#test = test_bkgd_fit(ph_events, energy_bins[0], time_bins, 
#                     bkgd_start+bkgd_stop, det_solid_angle)

# Set parameters for diffuse-only model
x0 = [float(parameters["phi_j"]), float(parameters["r_j"])]

# Fit background in off-time intervals
bkgd_off_init = [fit_background(ph_events, ebin, time_bins, x0, 
                                T=bkgd_start+bkgd_stop, omega_i=det_solid_angle) 
                                for ebin in energy_bins]

print ('\nBest-fit background parameters:')
print ('CXB', 'Det_Bkgd')
for e in range(len(energy_bins)):
    print ('Chan {} ({}, {} keV): {}, {}'.format(e, *energy_bins[e], *np.round(bkgd_off_init[e], 3)))


# Tito suggested NITRATES background fitting method
# might be an overkill here, so we can skip for now
finer_bkgd = False

if finer_bkgd is not False:
    print ('\nPerforming final background fits...')

    bkgd_off = []
    src_bins = np.arange(search_start, search_stop, step=1.024)

    for src_bin in src_bins:

        # Set pre and post background ranges
        # Take 30 seconds of background 
        # neglecting 10 seconds around center of src_bin
        pre_bnds  = (src_bin - 40, src_bin - 10)
        post_bnds = (src_bin + 10, src_bin + 40)
        new_bins  = np.arange(pre_bnds[0]-bkgd_res, post_bnds[1]+bkgd_res, 
                                step=bkgd_res) 

        # Set the time bins for the background
        time_bins = set_bkgd_bins(pre_bnds, post_bnds, 1.024, bins=new_bins)

        # Fit background around on-time intervals
        ## Need to add background_off_init parameters as initial guesses instead
        bkgd_off.append((src_bin, [fit_background(ph_events, ebin, time_bins, x0, 
                                                    T=bkgd_start+bkgd_stop, 
                                                    omega_i=det_solid_angle
                                                ) for ebin in energy_bins]
                        )
        )
    bkgd_off = np.array(bkgd_off, dtype=object)

print ('\nSetting up search windows...')

for timescale in timescales[:1]:
    windows = np.arange(search_start, search_stop, step=timescale)
    
    for window in windows[:1]:
        on_window = [window, window+timescale]
        on_center =  window + timescale / 2

        # Get closest best-fit background parameters
        try:
            bidx  = np.argmin(np.abs(bkgd_off[:,0] - on_center))
            bkgd_off_param = bkgd_off[bidx][1]
        except:
            bkgd_off_param = bkgd_off_init

        print (on_window, '\n')  

        # Cut data by time
        data_tbin = slice_by_time(ph_events, on_window)
        
        # Cut by energy
        for e in range(len(energy_bins)):
            data_ebin = slice_by_energy(data_tbin, energy_bins[e])

            # Bin by detector
            data = np.histogram2d(data_ebin["X"], data_ebin["Y"], bins=80)[0]
            
            # Set up GRB model
            src_params = fit_src_bkgd(data, bkgd_off_param[e], rmf_bnds,
                arf, irf, rmf, timescale, det_solid_angle, energy_bins[e])
            #print (src_params)
   
"""
"""

# Initialize background model counts
#bkgd_model_rate, _ = m.bkgd_only_model()
# Plot background model
#detector_plane_image(bkgd_model_rate, title=str(eb), show_rad=False)


# Show lightcurve (i.e., sum over detectors)
#pre_bgkd_cnts = 

# Linear fit to background data
bkgd_bins_lfit = np.concatenate((pre_bkgd_bins[:-1], post_bkgd_bins[:-1]))
bkgd_cnts_lfit = np.concatenate((pre_bkgd_cnts, post_bkgd_cnts))
bkgd_fit  = linregress(bkgd_bins_lfit, y=bkgd_cnts_lfit)

# Plot linear fit to background data
plt.stairs(pre_bkgd_cnts, edges=pre_bkgd_bins, 
                baseline=np.min(pre_bkgd_cnts), color='C0', 
                label='{}-{} keV'.format(eb[0], eb[1]))
plt.stairs(post_bkgd_cnts, edges=post_bkgd_bins, 
                baseline=np.min(post_bkgd_cnts), color='C0')
plt.plot(bkgd_bins_fit, bkgd_fit.intercept + bkgd_fit.slope * bkgd_bins_fit, 'r')
plt.legend(loc='upper right')
#plt.show()
plt.close()
"""

