import numpy as np
import matplotlib.pyplot as plt

#from point_source import comp, drm

def ll_diffuse_only(m, data, T, omega_i):
    """
    Want to minimize the negative log likelihood
    (maximize the log likelihood)
    for the diffuse-only background model

    m[0]: phi_j - solid angle detector rate
    m[1]: r_j   - detector background rate
    """
    lambda_diff_ij = T * (omega_i * m[0] + m[1])
    return -1 * np.sum(data * np.log(lambda_diff_ij) - lambda_diff_ij)


def comp(E, A, alpha, Epeak, Epiv=100):
    return A * (E/Epiv)**alpha * np.exp(-1 * (2 + alpha) * E / Epeak)


def ll_grb_and_diffuse_only(spec, data, bkgd, T, omega_i, chan_bins, arf, 
    irf, rmf, alpha=None, Epeak=None, i=None, j=None, ebounds=None):
    """
    Want to minimize the negative log likelihood
    (maximize the log likelihood)
    for the GRB model + diffuse background model

    bkgd[0]: phi_j - best fit solid angle detector rate
    bkgd[1]: r_j   - best fit detector background rate
    loc[0]: theta  - spacecraft coords
    loc[1]: phi    - spacecraft coords

    alpha: spectral index
    Epeak: peak of nuFnu spectrum
    """
    # Diffuse background
    lambda_diff_ij = omega_i * bkgd[0] + bkgd[1]

    # Set up incident photon energies based on RMF channel bounds
    dE = 0.125 # keV
    E_cent = np.array([row[1] + dE/2. for row in chan_bins])

    # GRB photon spectrum 
    photon_spectrum = comp(E_cent, spec[0], alpha, Epeak) * dE

    # Set up DRMS
    # Arbitrarily chose to split at 67 keV for the low and high IRF
    ph_per_sec = []
    for e in range(len(E_cent)):
        if e <= 520:
            r = arf[e] * irf[0][j][i]
        else:
            r = arf[e] * irf[1][j][i]
        ph_per_sec.append(photon_spectrum[e] * r)
    ph_per_sec = np.array(ph_per_sec)

    # Foward-fold through DRMs
    counts = np.dot(rmf.T, ph_per_sec)

    # Find energy channel bins
    for row in chan_bins:
        if row[1] >= ebounds[0]:
            ebnd_lo = row[0]
            break
    for row in chan_bins:
        if row[2] <= ebounds[1]:
            ebnd_hi = row[0]

    # Only take counts from the selected energy range
    lambda_grb_ij = np.sum(counts[ebnd_lo:ebnd_hi])

    # Background + source
    lambda_ij = T * (lambda_diff_ij + lambda_grb_ij)
    return -1 * np.sum(data * np.log(lambda_ij) - lambda_ij)
