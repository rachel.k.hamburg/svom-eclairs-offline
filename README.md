# ECLAIRs Subthreshold


## Simulating ECLAIRs Data

## Installing the SVOM Simulation Tools

1. Make a new python virtual environment `python -m venv /path/to/new/virtual/environment` 

2. Clone the `ecpi` git repo: 'git clone https://drf-gitlab.cea.fr/svom/ecpi.git'

3. Install the repo using instructions here: https://drf-gitlab.cea.fr/svom/ecpi and activate the `caldb` database

4. Clone `eclgrm-tools`: 'git clone https://drf-gitlab.cea.fr/svom/eclgrm/eclgrm-tools.git' 

5. Install Cython

6. `cd` into the directory and install the tools: `pip install -e .`

7. Set up environment variables: https://eclgrm-fr.drf-pages.cea.fr/eclgrm-documentation/prerequisites/env_variables.html 

8. Clone and install `eclgrm-fr` simulation tools: https://eclgrm-fr.drf-pages.cea.fr/eclgrm-documentation/projects/eclgrm-simulations/installation.html

9. Initiate pira-bg config using instructions here: https://eclgrm-fr.drf-pages.cea.fr/eclgrm-documentation/projects/pira-bkg/usage/config.html 

10. Download input data: `eclgrm-data-download`

11. Follow the rest of the instructions here: https://eclgrm-fr.drf-pages.cea.fr/eclgrm-documentation/prerequisites/download.html

Note: generating the "required files for `eclairs-gp` did not work. I received this error: 
```
Traceback (most recent call last):
  File "/Users/hamburg/venv/eclairs-subthreshold/bin/get-ecpi-inputs", line 5, in <module>
    from eclgrm_tools.scripts.ecpi import get_inputs
  File "/Users/hamburg/Dropbox/Work/SVOM/analysis/simulation_tools/eclgrm-tools/eclgrm_tools/scripts/ecpi.py", line 9, in <module>
    from eclgrm_tools.utils.ecpi_toolbox import generate_required_files_ecpi
  File "/Users/hamburg/Dropbox/Work/SVOM/analysis/simulation_tools/eclgrm-tools/eclgrm_tools/utils/ecpi_toolbox.py", line 11, in <module>
    from ecpi.extras.simu.create_bkg_nonuniformity_correction_files import (
ModuleNotFoundError: No module named 'ecpi.extras.simu.create_bkg_nonuniformity_correction_files'
```


## Using the SVOM Simulation Tools

1. First, create the attitude and background events files which you will insert the GRB. Use `Create_background_attitude_file.ipynb` in `eclgrm-simulations/notebooks/`. I will try 3 different attitude files:

a. Satellite_ephemerides/ephem_att_nominal_2020.txt
b. Satellite_ephemerides/ephem_att_nominal_2025.txt
c. Custom attitude file (as shown in `eclgrm-simulations/notebooks/Simulation_CEA_bkg.ipynb`)



1. Set the pira background config file: `pira-bkgd/pira_bkg_config.ini` (Read the PIRA paper for more information)

2. Simulate background based ont the config file: `pira-bkgd-sim` (may have to install curl first)

3. An events and attitude file are produced within the PIRA directory (I guess if you give that as your output directory?) You can view the background by plotting the events file as a lightcurve. You can also view the attitude file by plotting the position as a function of time.

4. `events simulation` simulates X-ray sources and GRBs and can merge the background and source simulations

5. There is a python notebook that gives and example of simulating a background + GRB source



## The Subthreshold Search

## Generating the Detector Responses
