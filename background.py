import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as plt

from calculate import ll_diffuse_only, ll_grb_and_diffuse_only

def slice_by_energy(ph_data, ebin, photon=True):
    if photon is not False:
        return ph_data[np.logical_and(ph_data["energy"] > ebin[0], 
                                        ph_data["energy"] < ebin[1])]
    
    return ph_data[np.logical_and(ph_data > ebin[0], 
                                    ph_data < ebin[1])]

def slice_by_time(ph_data, tbin, photon=True):
    if photon is not False:
        return ph_data[np.logical_and(ph_data["eventID"] >= tbin[0],
                                        ph_data["eventID"] <= tbin[1])]
    
    return ph_data[np.logical_and(ph_data >= tbin[0],
                                    ph_data <= tbin[1])]

def slice_bkgd(ph_data, ebin, tbin):
    """
    Slice the photon data by energy and time
    """
    ph_ebinned = slice_by_energy(ph_data, ebin)
    ph_tbinned_pre = slice_by_time(ph_ebinned, tbin[0])
    ph_tbinned_post = slice_by_time(ph_ebinned, tbin[1])
    return np.concatenate((ph_tbinned_pre, ph_tbinned_post))

def bin_by_time(start, stop, resolution):
    return np.arange(start, stop, step=resolution)

def set_bkgd_bins(pre_bounds, post_bounds, res, bins=None, off_time=False):
    if bins is None:
        bins = np.arange(0, 6000+res, step=res)

    pre_bins = slice_by_time(bins, pre_bounds, photon=False)
    post_bins = slice_by_time(bins, post_bounds, photon=False)
    
    #if off_time is not False:
    return [(pre_bins[0], pre_bins[-1]),(post_bins[0],post_bins[-1])]

def fit_bkgd_params(x0, data, T=1., omega_i=1., bounds=None, verbose=False):
    """
    Fits the background data to the background model parameters
    
    x0: np.array
        initial guesses of the fitting parameters (phi_j, r_j)
    """
    if bounds is None:
        bounds = ((0, None), (0, None)) # positive bounds

    result = minimize(ll_diffuse_only, x0, args=(data, T, omega_i), 
                        method='Nelder-Mead', bounds=bounds
    )
    if verbose is not False:
        print (result.fun)
    return result.x

def fit_background(ph_events, ebin, tbins, x0, **kwargs):

    # Slice by time and energy
    ph_bkgd = slice_bkgd(ph_events, ebin, tbins)
    
    # Bin by detector
    bkgd_data = np.histogram2d(ph_bkgd["X"], ph_bkgd["Y"], bins=80)[0]
    #detector_plane_image(bkgd_data, title=str(ebin), show_rad=False)

    # Fit background (diffuse only)
    return fit_bkgd_params(x0, bkgd_data, **kwargs)


def fit_src_bkgd(data, bkgd_param, ebins, arf, irf, rmf, T, omega_i,
    ebounds, pbounds=None, verbose=False):
    """
    Fits source+bkgd model to data
    """
    A = 1.
    alpha = -1.
    Epeak = 100.
    x0 = [A]

    # Set the bounds
    if pbounds is None:
        pbounds = (
            (0, None), # positive amplitude
            #(-3, 1),   # negative alpha (mostly)
            #(0, 10000)  # positive Epeak
        )
    ll_grb_and_diffuse_only(x0, data, bkgd_param, T, omega_i, ebins, arf, 
                            irf, rmf, alpha, Epeak, 99, 99, ebounds)

    #result = minimize(ll_grb_and_diffuse_only, x0, args=(data, bkgd_param, T, 
    #    omega_i, ebins, arf, irf, rmf, alpha, Epeak),
    #    method='Nelder-Mead', bounds=pbounds
    #)

    if verbose is not False:
        print (result.fun)
    return result.x



def test_bkgd_fit(data, ebin, tbin, T, omega_i):
    print ('Mapping likelihood surface...')
    
    # Slice by time and energy
    ph_bkgd = slice_bkgd(data, ebin, tbin)
    # Bin by detector
    bkgd_data = np.histogram2d(ph_bkgd["X"], ph_bkgd["Y"], bins=80)[0]

    # Set bounds/ranges
    phi_low = 0.
    phi_high = 2.
    r_low = 0.
    r_high = 2.

    # You can check the lower/upper/transpose plotting
    # with plt.imshow by changing the array size here
    phis = np.linspace(phi_low, phi_high, 200)
    rs = np.linspace(r_low, r_high, 200)

    # Calculate likelihood
    row = []
    for p in phis:
        col = []
        for r in rs:
            v = ll_diffuse_only([p,r], bkgd_data, T, omega_i)
            if v < 0:
                col.append(v)
            else:
                col.append(np.nan)
        row.append(col)
    row = np.array(row)

    # Plot likelihood surface
    plt.imshow(row.T, origin='lower', cmap='rainbow', 
               extent=(phi_low, phi_high, r_low, r_high))
    plt.colorbar(label='neg log likelihood')
    plt.scatter(0.79, 0, s=50, label='Positive bounds')
    plt.scatter(0.90, -0.044, s=50, label='No bounds')
    plt.xlabel('rate per detector solid angle (CXB)')
    plt.ylabel('rate per detector')
    plt.legend()
    plt.tight_layout()
    plt.show()
    plt.close()
    return


"""
def initial_bkgd_fit():
    bkgd_x0 = []

# Define initial background bins
bkgd_bnds = [pre_bkgd_bins[0], pre_bkgd_bins[-1], 
                 post_bkgd_bins[0], post_bkgd_bins[-1]]

for eb in energy_bins[:1]:
    
    # Slice background by energy and time
    ph_bkgd = bin_bkgd_data(ph_events, eb, bkgd_bnds)

    # Bin by detector
    bkgd_data_rate = np.histogram2d(ph_bkgd["X"], ph_bkgd["Y"], bins=80)[0]

    # Plot background data
    #detector_plane_image(bkgd_data_rate, title=str(eb), show_rad=False)

    # Fit background model to data (diffuse background only)
    x0 = [float(parameters['phi_j']), float(parameters['r_j'])]
    bkgd_T = bkgd_start + bkgd_stop
    result = minimize(ll_diffuse_only, x0, args=(bkgd_data_rate, bkgd_T, det_solid_angle), method='Nelder-Mead')#, bounds=bnds)#, options={'ftol':1e-6, 'gtol':1e-6}) 
    #print (result)
    #print (result.message)
    bkgd_x0.append(result.x)

bkgd_x0 = np.array(bkgd_x0)
    return

"""